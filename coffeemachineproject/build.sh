#!/bin/bash

mvn clean install -f commons/pom.xml

mvn clean install -f jdbcservice/pom.xml
docker build -f jdbcservice/Dockerfile ./jdbcservice --tag jdbcservice:master

mvn clean install -f remoteservice/pom.xml
docker build -f remoteservice/Dockerfile ./remoteservice --tag remoteservice:master

mvn clean install -f server/pom.xml
docker build -f server/Dockerfile ./server --tag server:master

mvn clean install -f client/pom.xml


docker build -f ldap/Dockerfile ./ldap --tag ldap:master

