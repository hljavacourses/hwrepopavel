package by.iba.hlteam.coffeemachineproject.jdbcservice;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Properties;

/**
 * JDBC server for executing SQL
 */
public class JdbcServiceServer {

    private static final String JDBC_EXCEPTION_START = "Unexpected exception during DB start";
    private static final String JDBC_EXCEPTION_STOP = "Unexpected exception during DB stop";
    private static final String JDBC_EXCEPTION_STATEMENT = "Unexpected exception during DB request";

    private static final String JDBC_CONFIG = "jdbcconfig.properties";

    private static final String HOST_PATH = "jdbc.host";
    private static final String LOGIN_PATH = "jdbc.login";
    private static final String PASSWORD_PATH = "jdbc.password";
    private static final String DRIVER_PATH = "jdbc.driver.class";

    private static Connection connection;

    /**
     * @return {@link PreparedStatement}
     */
    public static PreparedStatement getStatement(String SQL) {
        try {
            return connection.prepareStatement(SQL);
        } catch (Exception exp) {
            throw new RuntimeException(JDBC_EXCEPTION_STATEMENT);
        }
    }

    /**
     * Starts database
     */
    public static void start() {
        try {
            Properties properties = new Properties();
            properties.load(JdbcServiceServer.class.getClassLoader().getResourceAsStream(JDBC_CONFIG));

            String host = properties.getProperty(HOST_PATH);
            String login = properties.getProperty(LOGIN_PATH);
            String password = properties.getProperty(PASSWORD_PATH);

            // initializing sql driver
            Class.forName(properties.getProperty(DRIVER_PATH));
            // get connection
            connection = DriverManager.getConnection(host, login, password);
        } catch (Exception ex) {
            throw new RuntimeException(JDBC_EXCEPTION_START);
        }
    }

    /**
     * Stops database
     */
    public static void stop() {
        if (connection != null) {
            try {
                connection.close();
            } catch (SQLException e) {
                throw new RuntimeException(JDBC_EXCEPTION_STOP);
            }
        }
    }

    public static void main(String[] args) {
        JdbcServiceServer.start();
    }

}
