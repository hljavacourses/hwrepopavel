package by.iba.hlteam.coffeemachineproject.jdbcservice.dao;

import java.io.Serializable;
import java.sql.SQLException;
import java.util.List;

/**
 * Interface for DB operations
 */
public interface Dao<E> extends Serializable {
    void add(E obj) throws SQLException;
    void update(E obj) throws SQLException;
    void delete(E obj) throws SQLException;
    List<E> getAll() throws SQLException;
    E getById(int id) throws SQLException;
}
