package by.iba.hlteam.coffeemachineproject.remoteservice;

import org.apache.log4j.BasicConfigurator;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import java.io.IOException;
import java.rmi.AlreadyBoundException;
import java.rmi.NotBoundException;
import java.rmi.Remote;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.Hashtable;
import java.util.Properties;

import static javax.naming.Context.INITIAL_CONTEXT_FACTORY;
import static javax.naming.Context.PROVIDER_URL;

/**
 * RMI\JNDI service for remote method calls and object transfer
 */
public class RmiServiceServer {

    private static final String CONFIG_PROPERTIES = "remoteconfig.properties";
    private static final String RMI_PORT_PATH = "rmi.port";
    private static final String JNDI_URL_PATH = "jndi.url";
    private static final String CONTEXT_FACTORY_CLASS_PATH = "jndi.factory.class";

    private static final String RUNTIME_EXCEPTION_MSG = "Unexpected exception in Remote service";

    private static final Properties PROPERTIES = new Properties();

    // Log4J basic configuration and properties set up
    static {
        BasicConfigurator.configure();
        try {
            PROPERTIES.load(RmiServiceServer.class.getClassLoader().getResourceAsStream(CONFIG_PROPERTIES));
        } catch (IOException e) {
            throw new RuntimeException(RUNTIME_EXCEPTION_MSG);
        }
    }

    /**
     * @return port for RMI from {@see remoteconfig.properties}
     */
    private static int getPort() throws IOException {
        return Integer.parseInt(PROPERTIES.getProperty(RMI_PORT_PATH));
    }

    /**
     * @return URL for JNDI from {@see remoteconfig.properties}
     */
    private static String getURL() throws IOException {
        return PROPERTIES.getProperty(JNDI_URL_PATH);
    }

    /**
     * @return class path for JNDI factory from {@see remoteconfig.properties}
     */
    private static String getFactoryClassPath() throws IOException {
        return PROPERTIES.getProperty(CONTEXT_FACTORY_CLASS_PATH);
    }

    /**
     * @return configuired {@link InitialContext}
     */
    private static InitialContext getContext() throws IOException, NamingException {
        Hashtable<String, String> jndiProperties = new Hashtable<>();
        jndiProperties.put(INITIAL_CONTEXT_FACTORY, getFactoryClassPath());
        jndiProperties.put(PROVIDER_URL, getURL() + ":" + getPort());
        return new InitialContext(jndiProperties);
    }

    /**
     * @return configuired {@link Registry}
     */
    public static Registry getRmiRegistry() throws IOException, NamingException {
        return LocateRegistry.getRegistry(getPort());
    }

    /**
     * Binds an object to {@link InitialContext}
     */
    public static void bindJndi(String key, Object obj) throws NamingException, IOException {
        getContext().rebind(key, obj);
    }

    /**
     * Binds {@link Remote} object to RMI registry
     */
    public static void bindRmi(String key, Remote remote) throws NamingException, IOException, AlreadyBoundException {
        Remote remoteExportObject = UnicastRemoteObject.exportObject(remote, getPort());
        getRmiRegistry().rebind(key, remoteExportObject);
    }

    /**
     * @return {@link Object} from {@link InitialContext} by a key
     */
    public static Object lookupJndi(String key) throws NamingException, IOException {
        return getContext().lookup(key);
    }

    /**
     * @return {@link Remote} from RMI registry by a key
     */
    public static Remote lookupRmi(String key) throws NamingException, IOException, NotBoundException {
        return getRmiRegistry().lookup(key);
    }

    /**
     * Creates RMI Registry and {@link InitialContext}
     */
    public static void start() {
        try {
            LocateRegistry.createRegistry(getPort());
        } catch (Exception e) {
            throw new RuntimeException(RUNTIME_EXCEPTION_MSG);
        }
    }

    public static void main(String[] args) {
        RmiServiceServer.start();
    }

}
