package by.iba.hlteam.coffeemachineproject.client;

import by.iba.hlteam.coffeemachineproject.client.webcontainer.CustomLookupEjbContainer;
import by.iba.hlteam.coffeemachineproject.coffeemachine.annotations.CommandManagerInject;
import by.iba.hlteam.coffeemachineproject.coffeemachine.annotations.ExceptionHandlerInject;
import by.iba.hlteam.coffeemachineproject.coffeemachine.commandmanager.CommandManager;
import by.iba.hlteam.coffeemachineproject.coffeemachine.exceptionhandler.ExceptionHandling;

import javax.naming.NamingException;
import java.io.IOException;
import java.rmi.AlreadyBoundException;
import java.rmi.NotBoundException;
import java.util.concurrent.ExecutionException;

/**
 * Client side
 * TODO: Add UI
 */
public class ClientRunner {

    @CommandManagerInject
    private CommandManager commandManager;

    @ExceptionHandlerInject
    private ExceptionHandling exceptionHander;

    public static void main(String[] args) throws InstantiationException, IllegalAccessException, IOException, NotBoundException, NamingException, ExecutionException, InterruptedException, AlreadyBoundException {
        CustomLookupEjbContainer.getInstance(ClientRunner.class);
    }

}
