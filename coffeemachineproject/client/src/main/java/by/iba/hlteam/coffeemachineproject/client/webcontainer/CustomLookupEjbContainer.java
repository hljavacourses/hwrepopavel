package by.iba.hlteam.coffeemachineproject.client.webcontainer;

import by.iba.hlteam.coffeemachineproject.coffeemachine.annotations.CommandExec;
import by.iba.hlteam.coffeemachineproject.coffeemachine.annotations.CommandManagerInject;
import by.iba.hlteam.coffeemachineproject.coffeemachine.annotations.ExceptionHandlerInject;
import by.iba.hlteam.coffeemachineproject.coffeemachine.command.*;
import by.iba.hlteam.coffeemachineproject.coffeemachine.commandmanager.CommandManager;
import by.iba.hlteam.coffeemachineproject.coffeemachine.exceptionhandler.ExceptionHandling;
import by.iba.hlteam.coffeemachineproject.remoteservice.JndiServiceServer;
import net.sf.cglib.proxy.Enhancer;
import net.sf.cglib.proxy.MethodInterceptor;

import javax.naming.NamingException;
import java.io.IOException;
import java.lang.reflect.Field;
import java.rmi.AlreadyBoundException;

/**
 * Custom lookup EJB container
 */
public class CustomLookupEjbContainer {

    private static final String CREATE = "CREATE";
    private static final String READ = "READ";
    private static final String UPDATE = "UPDATE";
    private static final String DELETE = "DELETE";

    /**
     * @return proxy with initialized fields
     */
    public static <D> D getInstance(Class<D> clazz) throws IllegalAccessException, InstantiationException, NamingException, IOException, AlreadyBoundException {
        D instance = clazz.newInstance();
        for (Field field : clazz.getDeclaredFields()) {
            field.setAccessible(true);
            if (field.isAnnotationPresent(ExceptionHandlerInject.class)) {
                ExceptionHandling exceptionInstance = (ExceptionHandling) JndiServiceServer.lookupJndi(ExceptionHandling.class.getName());
                MethodInterceptor exceptionHandler = (obj, method, args, proxy) -> proxy.invoke(exceptionInstance, args);
                field.set(instance, Enhancer.create(ExceptionHandling.class, exceptionHandler));
            }
            if (field.isAnnotationPresent(CommandManagerInject.class)) {
                CommandManager managerInstance = (CommandManager) JndiServiceServer.lookupJndi(CommandManager.class.getName());
                MethodInterceptor managerHandler = (obj, method, args, proxy) -> proxy.invoke(managerInstance, args);
                field.set(instance, Enhancer.create(CommandManager.class, managerHandler));
            }
            if (field.isAnnotationPresent(CommandExec.class)) {
                CommandExec annotation = field.getAnnotation(CommandExec.class);
                Command command;
                switch(annotation.name()){
                    case CREATE:
                        command = (Command) JndiServiceServer.lookupJndi(CreateCommand.class.getName());
                        break;
                    case UPDATE:
                        command = (Command) JndiServiceServer.lookupJndi(UpdateCommand.class.getName());
                        break;
                    case DELETE:
                        command = (Command) JndiServiceServer.lookupJndi(DeleteCommand.class.getName());
                        break;
                    default:
                        command = (Command) JndiServiceServer.lookupJndi(ReadCommand.class.getName());
                        break;
                }
                MethodInterceptor managerHandler = (obj, method, args, proxy) -> proxy.invoke(command, args);
                field.set(instance, Enhancer.create(Command.class, managerHandler));
            }
        }
        return instance;
    }

}
