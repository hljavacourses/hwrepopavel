package com.example;

@Route
@PWA(name = "My TODO App", shortName = "My TODO")
public class AlexExample {

    public AlexExample() {

        Tabs tabs = new Tabs();
        tabs.add(createDetailsTab());
        tabs.add(createListTab());
        tabs.setOrientation(Orientation.VERTICAL);
        setJustifyContentMode(JustifyContentMode.CENTER);
        add(tabs);
    }

    private static Tab createListTab() {
        Tab tab = new Tab("All Customers");
        Grid<User> users = new Grid<>(User.class);
        Button readButton = new Button("Read");
        readButton.addClickListener(event -> {
            users.setItems(readUsers());
        });
        tab.add(users, readButton);

        return tab;
    }

    private static Tab createDetailsTab() {
        Tab tab = new Tab("Customer Details");

        FormLayout form = new FormLayout();

        // labels
        Label nameLabel = new Label("Customer Name");
        Label idLabel = new Label("Customer Identity");
        Label adressLabel = new Label("Customer Adress");

        // text fields
        TextField nameField = new TextField();
        TextField idField = new TextField();
        TextField adressField = new TextField();

        // fields
        HorizontalLayout customerName = new HorizontalLayout(nameLabel, nameField);
        HorizontalLayout customerId = new HorizontalLayout(idLabel, idField);
        HorizontalLayout customerAdress = new HorizontalLayout(adressLabel, adressField);
        VerticalLayout fields = new VerticalLayout(customerName, customerId, customerAdress);
        fields.setJustifyContentMode(JustifyContentMode.CENTER);

        TextArea log = new TextArea();
        log.setWidth("350px");
        log.setReadOnly(true);

        // buttons
        Button addButton = new Button("Add Customer");
        addButton.addClickListener(event -> {
            User user = new User(idField.getValue() == null ? Long.valueOf(idField.getValue()) : null, nameField.getValue(), adressField.getValue());
            User createdUser = createUser(user);
            log.setValue("Created - " + createdUser.toString());
        });
        Button getButton = new Button("Get Customer");
        Button updateButton = new Button("Update Customer");
        Button deleteButton = new Button("Delete Customer");

        HorizontalLayout firstButtonRow = new HorizontalLayout(addButton, updateButton);
        HorizontalLayout secondButtonRow = new HorizontalLayout(getButton, deleteButton);
        VerticalLayout buttons = new VerticalLayout(firstButtonRow, secondButtonRow);

        form.add(new VerticalLayout(fields, buttons, log));

        tab.add(form);

        return tab;
    }

    private static User createUser(User user) {
        User result = null;
        try {
            URL url = new URL("http://localhost:7000/create");
            HttpURLConnection con = (HttpURLConnection) url.openConnection();
            con.setRequestMethod("GET");

            con.setDoOutput(true);
            con.setDoInput(true);

            ObjectOutputStream outputStream = new ObjectOutputStream(con.getOutputStream());
            outputStream.writeObject(user);

            result = (User) new ObjectInputStream(con.getInputStream()).readObject();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;

    }

    private static List<User> readUsers() {
        List<User> result = new ArrayList<>();
        try {
            URL url = new URL("http://localhost:7000/read");
            HttpURLConnection con = (HttpURLConnection) url.openConnection();
            con.setRequestMethod("GET");

            con.setDoOutput(true);
            con.setDoInput(true);

            result = (List<User>) new ObjectInputStream(con.getInputStream()).readObject();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;

    }

}
