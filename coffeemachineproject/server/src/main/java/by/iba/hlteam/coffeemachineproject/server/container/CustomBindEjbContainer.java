package by.iba.hlteam.coffeemachineproject.server.container;

import by.iba.hlteam.coffeemachineproject.coffeemachine.annotations.CommandManagerInject;
import by.iba.hlteam.coffeemachineproject.coffeemachine.annotations.ExceptionHandlerInject;
import by.iba.hlteam.coffeemachineproject.coffeemachine.commandmanager.CommandManager;
import by.iba.hlteam.coffeemachineproject.coffeemachine.exceptionhandler.ExceptionHandling;
import by.iba.hlteam.coffeemachineproject.remoteservice.JndiServiceServer;
import by.iba.hlteam.coffeemachineproject.server.command.CreateCommandImpl;
import by.iba.hlteam.coffeemachineproject.server.command.DeleteCommandImpl;
import by.iba.hlteam.coffeemachineproject.server.command.ReadCommandImpl;
import by.iba.hlteam.coffeemachineproject.server.command.UpdateCommandImpl;
import by.iba.hlteam.coffeemachineproject.server.command.manager.CommandManagerImpl;
import by.iba.hlteam.coffeemachineproject.server.exceptionhandler.ExceptionHandler;
import net.sf.cglib.proxy.Enhancer;
import net.sf.cglib.proxy.MethodInterceptor;

import javax.naming.NamingException;
import java.io.IOException;
import java.lang.reflect.Field;
import java.rmi.AlreadyBoundException;

/**
 * Custom bind EJB container
 */
public class CustomBindEjbContainer {

    /**
     * @return proxy with initialized fields
     */
    public static <D> D getInstance(Class<D> clazz) throws IllegalAccessException, InstantiationException, NamingException, IOException, AlreadyBoundException {
        D instance = clazz.newInstance();
        for (Field field : clazz.getDeclaredFields()) {
            field.setAccessible(true);
            if (field.isAnnotationPresent(ExceptionHandlerInject.class)) {
                ExceptionHandling exceptionInstance = new ExceptionHandler();
                MethodInterceptor exceptionHandler = (obj, method, args, proxy) -> proxy.invoke(exceptionInstance, args);
                field.set(instance, Enhancer.create(ExceptionHandler.class, exceptionHandler));
                JndiServiceServer.bindJndi(ExceptionHandling.class.getName(), exceptionInstance);
            }
            if (field.isAnnotationPresent(CommandManagerInject.class)) {
                CommandManager managerInstance = new CommandManagerImpl();
                MethodInterceptor managerHandler = (obj, method, args, proxy) -> proxy.invoke(managerInstance, args);
                field.set(instance, Enhancer.create(CommandManagerImpl.class, managerHandler));
                JndiServiceServer.bindJndi(CommandManager.class.getName(), managerInstance);
            }
        }

        JndiServiceServer.bindJndi(CommandManager.class.getName(), new CreateCommandImpl());
        JndiServiceServer.bindJndi(CommandManager.class.getName(), new UpdateCommandImpl());
        JndiServiceServer.bindJndi(CommandManager.class.getName(), new DeleteCommandImpl());
        JndiServiceServer.bindJndi(CommandManager.class.getName(), new ReadCommandImpl());

        return instance;
    }

}
