package by.iba.hlteam.coffeemachineproject.server.command;

import by.iba.hlteam.coffeemachineproject.coffeemachine.command.ReadCommand;
import by.iba.hlteam.coffeemachineproject.coffeemachine.model.user.User;
import by.iba.hlteam.coffeemachineproject.coffeemachine.model.user.UserTO;
import by.iba.hlteam.coffeemachineproject.jdbcservice.dao.Dao;
import by.iba.hlteam.coffeemachineproject.server.dao.UserDao;

import java.rmi.RemoteException;
import java.sql.SQLException;

/**
 * Implementation of read command
 */
public class ReadCommandImpl implements ReadCommand {

    private static final long serialVersionUID = 7889848035006314174L;

    private Dao<User> dao = new UserDao();

    @Override
    public UserTO execute(UserTO object) throws RemoteException {
        UserTO userTO;
        try {
            userTO = dao.getById(object.getId());
        } catch (SQLException e) {
            // TODO: Implement exception handling
            throw new RemoteException(e.getMessage());
        }
        return userTO;
    }

}
