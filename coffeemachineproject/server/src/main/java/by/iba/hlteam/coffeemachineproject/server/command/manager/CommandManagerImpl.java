package by.iba.hlteam.coffeemachineproject.server.command.manager;

import by.iba.hlteam.coffeemachineproject.server.command.CreateCommandImpl;
import by.iba.hlteam.coffeemachineproject.server.command.DeleteCommandImpl;
import by.iba.hlteam.coffeemachineproject.server.command.ReadCommandImpl;
import by.iba.hlteam.coffeemachineproject.server.command.UpdateCommandImpl;
import by.iba.hlteam.coffeemachineproject.coffeemachine.command.*;
import by.iba.hlteam.coffeemachineproject.coffeemachine.commandmanager.CommandManager;
import by.iba.hlteam.coffeemachineproject.coffeemachine.model.TransferObject;

import java.rmi.RemoteException;
import java.util.Hashtable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

/**
 * Implementation of command manager
 */
public class CommandManagerImpl implements CommandManager {

    private static final long serialVersionUID = 8972535832264668334L;

    private static final String MANAGER_EXCEPTION_MSG = "Commands are not specified";

    /**
     * Thread executor for commands
     */
    private transient ExecutorService executorService = Executors.newFixedThreadPool(100);

    /**
     * Contains pairs of Class - {@link Command} implementations
     */
    private Hashtable<Class, Command> commands = new Hashtable<Class, Command>() {
        private static final long serialVersionUID = -1423558596597054026L;

        {
            put(CreateCommand.class, new CreateCommandImpl());
            put(UpdateCommand.class, new UpdateCommandImpl());
            put(ReadCommand.class, new ReadCommandImpl());
            put(DeleteCommand.class, new DeleteCommandImpl());
        }
    };

    @Override
    public <T, D extends TransferObject> D execute(Class<T> clazz, D obj) throws RemoteException, ExecutionException, InterruptedException {
        if(commands == null || commands.isEmpty()){
            throw new InterruptedException(MANAGER_EXCEPTION_MSG);
        }
        Command<D> command = commands.get(clazz);
        Future<D> future = executorService.submit(() -> command.execute(obj));
        return future.get();
    }

}
