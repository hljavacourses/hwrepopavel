package by.iba.hlteam.coffeemachineproject.server.command;

import by.iba.hlteam.coffeemachineproject.coffeemachine.command.CreateCommand;
import by.iba.hlteam.coffeemachineproject.coffeemachine.model.user.User;
import by.iba.hlteam.coffeemachineproject.coffeemachine.model.user.UserTO;
import by.iba.hlteam.coffeemachineproject.jdbcservice.dao.Dao;
import by.iba.hlteam.coffeemachineproject.server.dao.UserDao;

import java.rmi.RemoteException;
import java.sql.SQLException;

/**
 * Implementation of create command
 */
public class CreateCommandImpl implements CreateCommand {

    private static final long serialVersionUID = -2105013413428429329L;

    private Dao<User> dao = new UserDao();

    @Override
    public UserTO execute(UserTO object) throws RemoteException {
        try {
            dao.add(new User(object.getId(), object.getUsername(), object.getPassword()));
        } catch (SQLException e) {
            // TODO: Implement exception handling
            throw new RemoteException(e.getMessage());
        }
        return object;
    }

}
