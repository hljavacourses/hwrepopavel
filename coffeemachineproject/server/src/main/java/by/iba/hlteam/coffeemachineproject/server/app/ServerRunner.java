package by.iba.hlteam.coffeemachineproject.server.app;

import by.iba.hlteam.coffeemachineproject.coffeemachine.annotations.CommandManagerInject;
import by.iba.hlteam.coffeemachineproject.coffeemachine.annotations.ExceptionHandlerInject;
import by.iba.hlteam.coffeemachineproject.coffeemachine.commandmanager.CommandManager;
import by.iba.hlteam.coffeemachineproject.coffeemachine.exceptionhandler.ExceptionHandling;
import by.iba.hlteam.coffeemachineproject.jdbcservice.JdbcServiceServer;
import by.iba.hlteam.coffeemachineproject.server.container.CustomBindEjbContainer;

/**
 * BUILD (You can run {@see build.bat} file to run the build):
 * 1) {@see commons}
 * 2) {@see jdbcservice}
 * 3) {@see remoteservice}
 * 4) {@see server}
 * 5) {@see client}
 * <p>
 * NOTE: Before start check {@see remoteconfig.properties} in {@see remoteservice} module,
 * {@see jdbcconfig.properties} in {@see jdbcservice} module
 */
public class ServerRunner {

    /**
     * Custom exception handler
     */
    @ExceptionHandlerInject
    private ExceptionHandling exceptionHandling;
    /**
     * Command manager to proceed user's commands
     */
    @CommandManagerInject
    private CommandManager commandManager;

    @Override
    protected void finalize() throws Throwable {
        JdbcServiceServer.stop();
    }

    public static void main(String[] args) throws Exception {
        CustomBindEjbContainer.getInstance(ServerRunner.class);
    }

}
