package by.iba.hlteam.coffeemachineproject.server.exceptionhandler;

import by.iba.hlteam.coffeemachineproject.coffeemachine.exceptionhandler.ExceptionHandling;

import javax.naming.NamingException;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;

/**
 * Handles exceptions and throws {@link RuntimeException}
 */
public class ExceptionHandler implements ExceptionHandling {

    private static final long serialVersionUID = -5351292284672457307L;

    /**
     * Handles:
     * {@link RemoteException},
     * {@link NamingException},
     * {@link ClassNotFoundException},
     * {@link NullPointerException},
     * {@link NotBoundException},
     * {@link InterruptedException}
     */
    @Override
    public void throwException(Exception e) throws RemoteException {
        if (e instanceof RemoteException ||
                e instanceof NamingException ||
                e instanceof ClassNotFoundException ||
                e instanceof NullPointerException ||
                e instanceof NotBoundException ||
                e instanceof InterruptedException) {
            throw new RuntimeException("Unexpected exception: " + e.getMessage());
        }
    }

}
