package by.iba.hlteam.coffeemachineproject.server.dao;

import by.iba.hlteam.coffeemachineproject.coffeemachine.model.user.User;
import by.iba.hlteam.coffeemachineproject.jdbcservice.JdbcServiceServer;
import by.iba.hlteam.coffeemachineproject.jdbcservice.dao.Dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Implementations for DB operations
 */
public class UserDao implements Dao<User> {

    private static final long serialVersionUID = -2046089136657763945L;

    private static final String INSERT_STATEMENT = "INSERT INTO user(username,password) VALUES(?,?)";
    private static final String UPDATE_STATEMENT = "UPDATE user SET username = ?, password = ? WHERE id = ?";
    private static final String DELETE_STATEMENT = "DELETE FROM user WHERE id = ?";
    private static final String READ_ALL_STATEMENT = "SELECT * FROM user";
    private static final String GET_BY_ID_STATEMENT = "SELECT * FROM user WHERE id = ?";

    private static final String ID_COLUMN = "id";
    private static final String USERNAME_COLUMN = "username";
    private static final String PASSWORD_COLUMN = "password";

    @Override
    public void add(User obj) throws SQLException {
        PreparedStatement statement = JdbcServiceServer.getStatement(INSERT_STATEMENT);
        statement.setString(1, obj.getUsername());
        statement.setString(2, obj.getPassword());
        statement.executeUpdate();
    }

    @Override
    public void update(User obj) throws SQLException {
        PreparedStatement statement = JdbcServiceServer.getStatement(UPDATE_STATEMENT);
        statement.setString(1, obj.getUsername());
        statement.setString(2, obj.getPassword());
        statement.setInt(3, obj.getId());
        statement.executeUpdate();
    }

    @Override
    public void delete(User obj) throws SQLException {
        PreparedStatement statement = JdbcServiceServer.getStatement(DELETE_STATEMENT);
        statement.setInt(1, obj.getId());
        statement.executeUpdate();
    }

    @Override
    public List<User> getAll() throws SQLException {
        PreparedStatement statement = JdbcServiceServer.getStatement(READ_ALL_STATEMENT);
        ResultSet string = statement.executeQuery();

        List<User> users = new ArrayList<>();
        while (string.next()) {
            User user = new User(string.getInt(ID_COLUMN), string.getString(USERNAME_COLUMN), string.getString(PASSWORD_COLUMN));
            users.add(user);
        }
        return users;
    }

    @Override
    public User getById(int id) throws SQLException {
        PreparedStatement statement = JdbcServiceServer.getStatement(GET_BY_ID_STATEMENT);
        statement.setInt(1, id);
        ResultSet string = statement.executeQuery();

        User user = null;
        while (string.next()) {
            user = new User(string.getInt(ID_COLUMN), string.getString(USERNAME_COLUMN), string.getString(PASSWORD_COLUMN));
        }
        return user;
    }

}
