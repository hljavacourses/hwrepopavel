package by.iba.hlteam.coffeemachineproject.coffeemachine.command;

import by.iba.hlteam.coffeemachineproject.coffeemachine.model.user.UserTO;

/**
 * Delete command interface that client uses
 */
public interface DeleteCommand extends Command<UserTO> {

}
