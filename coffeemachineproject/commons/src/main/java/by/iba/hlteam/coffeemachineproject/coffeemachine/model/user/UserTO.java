package by.iba.hlteam.coffeemachineproject.coffeemachine.model.user;

import by.iba.hlteam.coffeemachineproject.coffeemachine.model.TransferObject;

import java.rmi.RemoteException;

/**
 * User transfer object
 */
public interface UserTO extends TransferObject {
    int getId() throws RemoteException;
    void setId(int id) throws RemoteException;
    String getUsername() throws RemoteException;
    void setUsername(String username) throws RemoteException;
    String getPassword() throws RemoteException;
    void setPassword(String password) throws RemoteException;
}
