package by.iba.hlteam.coffeemachineproject.coffeemachine.command;

import by.iba.hlteam.coffeemachineproject.coffeemachine.model.user.UserTO;

/**
 * Create command interface that client uses
 */
public interface CreateCommand extends Command<UserTO> {

}
