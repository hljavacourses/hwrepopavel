package by.iba.hlteam.coffeemachineproject.coffeemachine.command;

import by.iba.hlteam.coffeemachineproject.coffeemachine.model.user.UserTO;

/**
 * Update command interface that client uses
 */
public interface UpdateCommand extends Command<UserTO> {

}
