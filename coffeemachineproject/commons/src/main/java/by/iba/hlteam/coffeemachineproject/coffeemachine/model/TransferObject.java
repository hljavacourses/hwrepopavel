package by.iba.hlteam.coffeemachineproject.coffeemachine.model;

import java.io.Serializable;
import java.rmi.Remote;

/**
 * Interface to identify transfer object
 */
public interface TransferObject extends Remote, Serializable {

}
