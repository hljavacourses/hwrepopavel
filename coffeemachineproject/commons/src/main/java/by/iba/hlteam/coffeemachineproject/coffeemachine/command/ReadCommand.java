package by.iba.hlteam.coffeemachineproject.coffeemachine.command;

import by.iba.hlteam.coffeemachineproject.coffeemachine.model.user.UserTO;

/**
 * Read command interface that client uses
 */
public interface ReadCommand  extends Command<UserTO> {

}
