package by.iba.hlteam.coffeemachineproject.coffeemachine.commandmanager;

import by.iba.hlteam.coffeemachineproject.coffeemachine.model.TransferObject;

import java.io.Serializable;
import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.concurrent.ExecutionException;

/**
 * Interface that identifies command manager
 */
public interface CommandManager extends Remote, Serializable {
    <T, D extends TransferObject> D execute(final Class<T> clazz, D obj) throws RemoteException, ExecutionException, InterruptedException;
}
