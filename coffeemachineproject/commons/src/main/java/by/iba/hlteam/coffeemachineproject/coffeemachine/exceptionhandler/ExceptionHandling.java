package by.iba.hlteam.coffeemachineproject.coffeemachine.exceptionhandler;

import java.io.Serializable;
import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 * An interface for custom exception handlers
 */
public interface ExceptionHandling extends Remote, Serializable {
    void throwException(Exception e) throws RemoteException;
}
