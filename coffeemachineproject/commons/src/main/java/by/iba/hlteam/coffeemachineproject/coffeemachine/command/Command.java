package by.iba.hlteam.coffeemachineproject.coffeemachine.command;

import java.io.Serializable;
import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 * Interface that identifies command behavior
 */
public interface Command<T> extends Remote, Serializable {
    T execute(T object) throws RemoteException;
}
